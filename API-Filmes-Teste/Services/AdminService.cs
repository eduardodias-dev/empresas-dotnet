﻿using API_Filmes_Teste.Models.DTO;
using API_Filmes_Teste.Pagination;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Filmes_Teste.Services
{
    public class AdminService : IAdminService
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public AdminService(SignInManager<IdentityUser> signInManager,
            UserManager<IdentityUser> userManager,
            RoleManager<IdentityRole> roleManager
        )
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _roleManager = roleManager;

        }
        public async Task CreateUser(UserDTO user)
        {
            IdentityUser identityUser = new IdentityUser
            {
                UserName = user.UserName,
                Email = user.Email,
                EmailConfirmed = true,
                LockoutEnabled = false,

            };
            IdentityResult result = await _userManager.CreateAsync(identityUser, user.Password);

            if (!result.Succeeded)
            {

                throw new Exception(JsonConvert.SerializeObject(result.Errors));
            }

            await _signInManager.SignInAsync(identityUser, false);

            identityUser = await _userManager.FindByNameAsync(user.UserName);
            await CreateRoles();

            IdentityRole role = await _roleManager.FindByNameAsync("Admin");
            await _userManager.AddToRoleAsync(identityUser, "Admin");

        }

        public async Task DeleteUser(string id)
        {
            IdentityUser identityUser = await _userManager.FindByIdAsync(id);
            identityUser.LockoutEnabled = true;

            IdentityResult result = await _userManager.UpdateAsync(identityUser);

            if (!result.Succeeded)
            {

                throw new Exception(JsonConvert.SerializeObject(result.Errors));
            }
        }

        public async Task<PagedList<UserDTO>> GetUsers(PageParameters parameters)
        {
            IList<IdentityUser> identityUsers = await _userManager.GetUsersInRoleAsync("User");
            List<UserDTO> users = new List<UserDTO>();

            foreach (var user in identityUsers)
            {
                if (!user.LockoutEnabled)
                {
                    bool isAdmin = await _userManager.IsInRoleAsync(user, "Admin");
                    bool isUser = await _userManager.IsInRoleAsync(user, "User");

                    if (isAdmin || isUser)
                    {
                        UserDTO userDTO = new UserDTO
                        {
                            Id = user.Id,
                            UserName = user.UserName,
                            Email = user.Email,
                            Role = isAdmin ? "Admin" : "User"
                        };

                        users.Add(userDTO);

                    }
                }
            }

            PagedList<UserDTO> pagedUsers = PagedList<UserDTO>.ToPagedList(users.OrderBy(x => x.UserName).AsQueryable(), parameters.PageNumber, parameters.PageSize);

            return pagedUsers;
        }

        public async Task UpdateUser(string id, UserDTO user)
        {
            IdentityUser identityUser = await _userManager.FindByIdAsync(id);
            if (identityUser == null)
            {
                throw new Exception("User not found.");
            }
            bool isAdmin = await _userManager.IsInRoleAsync(identityUser, "Admin");
            if (!isAdmin)
            {
                throw new Exception("User is not admin");
            }
            identityUser.Id = user.Id;
            identityUser.UserName = user.UserName;
            identityUser.Email = user.Email;
            identityUser.EmailConfirmed = true;
            identityUser.LockoutEnabled = false;

            IdentityResult result = await _userManager.UpdateAsync(identityUser);

            if (!result.Succeeded)
            {
                throw new Exception(JsonConvert.SerializeObject(result.Errors));
            }

        }

        private async Task CreateRoles()
        {
            string[] roleNames = { "Admin", "User" };
            IdentityResult roleResult;

            foreach (var roleName in roleNames)
            {
                var roleExist = await _roleManager.RoleExistsAsync(roleName);
                if (!roleExist)
                {
                    //create the roles and seed them to the database: Question 2
                    roleResult = await _roleManager.CreateAsync(new IdentityRole(roleName));
                }
            }
        }
    }
}
