﻿using API_Filmes_Teste.Models.DTO;
using API_Filmes_Teste.Pagination;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace API_Filmes_Teste.Services
{
    public class UserService : IUserService
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private IConfiguration _configuration;

        public UserService(SignInManager<IdentityUser> signInManager,
            UserManager<IdentityUser> userManager,
            RoleManager<IdentityRole> roleManager,
            IConfiguration configuration
        )
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _roleManager = roleManager;
            _configuration = configuration;

        }
        public async Task CreateUser(UserDTO user)
        {
            IdentityUser identityUser = new IdentityUser
            {
                UserName = user.UserName,
                Email = user.Email,
                EmailConfirmed = true,
                LockoutEnabled = false,

            };
            IdentityResult result = await _userManager.CreateAsync(identityUser, user.Password);

            if (!result.Succeeded)
            {

                throw new Exception(JsonConvert.SerializeObject(result.Errors));
            }

            identityUser = await _userManager.FindByNameAsync(user.UserName);

            await _userManager.AddToRoleAsync(identityUser, "User");
        }

        public async Task DeleteUser(string id)
        {
            IdentityUser identityUser = await _userManager.FindByIdAsync(id);
            identityUser.LockoutEnabled = true;

            IdentityResult result = await _userManager.UpdateAsync(identityUser);

            if (!result.Succeeded)
            {

                throw new Exception(JsonConvert.SerializeObject(result.Errors));
            }
        }

        public UserToken GenerateToken(UserDTO user)
        {
            user.Role = String.IsNullOrEmpty(user.Role) ? "User" : "Admin";
            Claim[] claims = new Claim[]
            {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.Role, user.Role),
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString())
            };

            var expirationDate = DateTime.Now.AddHours(double.Parse(_configuration["TokenConfiguration:ExpirationHours"]));
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            JwtSecurityToken token = new JwtSecurityToken
            (
                issuer: _configuration["TokenConfiguration:Issuer"],
                audience: _configuration["TokenConfiguration:Audience"],
                signingCredentials: credentials,
                expires: expirationDate,
                claims: claims
            );

            return new UserToken
            {
                Authenticated = true,
                Message = "Token gerado com sucesso!",
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                Expiration = expirationDate,
                UserName = user.UserName
            };
        }

        public async Task<UserToken> Login(UserDTO user)
        {
            var result = await _signInManager.PasswordSignInAsync(user.UserName, user.Password, true, false);

            if (result.Succeeded)
            {
                return GenerateToken(user);
            }
            else
            {
                throw new Exception("Usuário e/ou senha inválidos.");
            }
        }

        public async Task UpdateUser(string id, UserDTO user)
        {
            IdentityUser identityUser = await _userManager.FindByIdAsync(id);
            if (identityUser == null)
            {
                throw new Exception("User not found.");
            }
            identityUser.UserName = user.UserName;
            identityUser.Email = user.Email;
            identityUser.LockoutEnabled = user.Deleted;

            IdentityResult result = await _userManager.UpdateAsync(identityUser);

            if (!result.Succeeded)
            {
                throw new Exception(JsonConvert.SerializeObject(result.Errors));
            }

            IdentityRole role = await _roleManager.FindByNameAsync("User");
            await _userManager.AddToRoleAsync(identityUser, "User");

        }
    }
}
