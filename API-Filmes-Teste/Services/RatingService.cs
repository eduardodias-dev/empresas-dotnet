﻿using API_Filmes_Teste.Data.Repository;
using API_Filmes_Teste.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace API_Filmes_Teste.Services
{
    public class RatingService : IRatingService
    {
        private readonly IRatingRepository _ratingRepository;
        public RatingService(IRatingRepository ratingRepository)
        {
            _ratingRepository = ratingRepository;
        }
        public void Add(Rating entity)
        {
            _ratingRepository.Add(entity);
            Commit();
        }

        public IQueryable<Rating> Get()
        {
            return _ratingRepository.Get();
        }

        public Rating GetByExpression(Expression<Func<Rating, bool>> expression)
        {
            return _ratingRepository.GetByExpression(expression);
        }

        public Rating GetById(int id)
        {
            return _ratingRepository.GetByExpression(x => x.RatingId == id);
        }

        public void Remove(Rating entity)
        {
            _ratingRepository.Remove(entity);
            Commit();
        }

        public void Update(Rating entity)
        {
            _ratingRepository.Update(entity);
            Commit();
        }

        public void Commit()
        {
            _ratingRepository.Commit();
        }

        
    }
}
