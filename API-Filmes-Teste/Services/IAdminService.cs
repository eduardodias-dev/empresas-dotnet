﻿using API_Filmes_Teste.Models.DTO;
using API_Filmes_Teste.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Filmes_Teste.Services
{
    public interface IAdminService
    {
        Task<PagedList<UserDTO>> GetUsers(PageParameters parameters);
        Task UpdateUser(string id, UserDTO user);
        Task CreateUser(UserDTO user);
        Task DeleteUser(string id);
    }
}
