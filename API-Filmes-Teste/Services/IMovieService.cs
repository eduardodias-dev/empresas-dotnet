﻿using API_Filmes_Teste.Models;
using API_Filmes_Teste.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Filmes_Teste.Services
{
    public interface IMovieService : IService<Movie>
    {
        PagedList<Movie> GetAllMoviesPaged(PageParameters parameters);
    }
}
