﻿using API_Filmes_Teste.Data.Repository;
using API_Filmes_Teste.Models;
using API_Filmes_Teste.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace API_Filmes_Teste.Services
{
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _movieRepository;
        public MovieService(IMovieRepository movieRepository)
        {
            _movieRepository = movieRepository;
        }
        public void Add(Movie entity)
        {
            _movieRepository.Add(entity);
            Commit();
        }

        public IQueryable<Movie> Get()
        {
            return _movieRepository.Get();
        }

        public PagedList<Movie> GetAllMoviesPaged(PageParameters parameters)
        {
            return _movieRepository.GetAllMoviesPaged(parameters);
        }

        public Movie GetByExpression(Expression<Func<Movie, bool>> expression)
        {
            return _movieRepository.GetByExpression(expression);
        }

        public Movie GetById(int id)
        {
            return _movieRepository.GetMovieDetail(x => x.MovieId == id);
        }

        public void Remove(Movie entity)
        {
            _movieRepository.Remove(entity);
            Commit();
        }

        public void Update(Movie entity)
        {
            _movieRepository.Update(entity);
            Commit();
        }

        public void Commit()
        {
            _movieRepository.Commit();
        }
    }
}
