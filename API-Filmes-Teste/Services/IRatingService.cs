﻿using API_Filmes_Teste.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Filmes_Teste.Services
{
    public interface IRatingService : IService<Rating>
    {
    }
}
