﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Filmes_Teste.Migrations
{
    public partial class CorrectUserTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Confirm",
                table: "User",
                newName: "ConfirmPassword");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ConfirmPassword",
                table: "User",
                newName: "Confirm");
        }
    }
}
