﻿using API_Filmes_Teste.Models;
using API_Filmes_Teste.Models.DTO;
using API_Filmes_Teste.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Filmes_Teste.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorizeController : ControllerBase
    {
        private readonly IUserService _userService;

        public AuthorizeController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Realiza o login e gera o token, necessário para acessar as outras funcionalidades.
        /// </summary>
        /// <param name="user">Usuário que irá logar.</param>
        /// <returns></returns>
        [HttpPost("login")]
        public async Task<ActionResult> Login(UserDTO user)
        {
            try
            {
                UserToken userToken = await _userService.Login(user);

                return Ok(userToken);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

    }
}
