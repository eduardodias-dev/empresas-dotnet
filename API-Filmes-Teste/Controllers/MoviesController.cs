﻿using API_Filmes_Teste.Models;
using API_Filmes_Teste.Pagination;
using API_Filmes_Teste.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Filmes_Teste.Controllers
{
    /// <summary>
    /// Operações de Filmes
    /// </summary>
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieService _movieService;
        private readonly IRatingService _ratingService;
        public MoviesController(IMovieService movieService, IRatingService ratingService)
        {
            _movieService = movieService;
            _ratingService = ratingService;
        }
        /// <summary>
        /// Listagem de filmes, com paginação
        /// </summary>
        /// <param name="parameters">Parametros de paginação</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<PagedList<Movie>> Get([FromQuery]PageParameters parameters)
        {
            try
            {
                PagedList<Movie> movies = _movieService.GetAllMoviesPaged(parameters);

                var paginationInfo = new
                {
                    movies.TotalCount,
                    movies.PageSize,
                    movies.CurrentPage,
                    movies.TotalPages,
                    movies.HasNext,
                    movies.HasPrevious
                };

                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationInfo));

                return movies;
            }catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        /// <summary>
        /// Detalhes do filme, incluindo os votos
        /// </summary>
        /// <param name="id">Id do filme</param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetMovieById")]
        public ActionResult<Movie> Get(int id)
        {
            try
            {
                return _movieService.GetById(id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        /// <summary>
        /// Cadastro de filmes, apenas para administradores.
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult<Movie> Post([FromBody] Movie movie)
        {
            try
            {
                _movieService.Add(movie);

                return new CreatedAtRouteResult("GetMovieById", new { id = movie.MovieId }, movie);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Insere um voto de 0-4 para o filme especificado.
        /// </summary>
        /// <param name="rating"></param>
        /// <returns></returns>
        [HttpPost("rate")]
        [Authorize(Roles = "Admin, User")]
        public ActionResult<Movie> Post([FromBody] Rating rating)
        {
            try
            {
                _ratingService.Add(rating);

                return new CreatedAtRouteResult("GetMovieById", new { id = rating.MovieId });
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

    }
}
