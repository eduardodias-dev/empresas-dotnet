﻿using API_Filmes_Teste.Models;
using API_Filmes_Teste.Models.DTO;
using API_Filmes_Teste.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace API_Filmes_Teste.Controllers
{
    /// <summary>
    /// Operações de usuário
    /// </summary>
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService
        )
        {
            _userService = userService;

        }

        /// <summary>
        /// Cadastra um usuário.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Admin, User")]
        public async Task<ActionResult> Post([FromBody] UserDTO user)
        {
            try
            {
                await _userService.CreateUser(user);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Edita um usuário existente.
        /// </summary>
        /// <param name="id">Id do usuário</param>
        /// <param name="user">Dados do Usuário</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize(Roles = "Admin, User")]
        public async Task<ActionResult> Put(string id, [FromBody] UserDTO user)
        {
            try
            {
                await _userService.UpdateUser(id, user);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Exclui um usuário do sistema.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin, User")]
        public async Task<ActionResult> Delete(string id)
        {
            try
            {
                await _userService.DeleteUser(id);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
