﻿using API_Filmes_Teste.Models;
using API_Filmes_Teste.Models.DTO;
using API_Filmes_Teste.Pagination;
using API_Filmes_Teste.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace API_Filmes_Teste.Controllers
{
    /// <summary>
    /// Operações de usuários administradores
    /// </summary>
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    [ApiController]
    public class AdministratorController : ControllerBase
    {
        private readonly IAdminService _adminService;

        public AdministratorController(IAdminService adminService
        )
        {
            _adminService = adminService;
        }
        /// <summary>
        /// Lista Usuários não administradores de forma paginada (Apenas para administradores)
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<PagedList<UserDTO>>> Get([FromQuery]PageParameters parameters)
        {
            try
            {
                PagedList<UserDTO> users = await _adminService.GetUsers(parameters);

                var paginationInfo = new
                {
                    users.TotalCount,
                    users.PageSize,
                    users.CurrentPage,
                    users.TotalPages,
                    users.HasNext,
                    users.HasPrevious
                };

                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationInfo));

                return users;

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Cadastra um usuário administrador. (Apenas para administradores)
        /// </summary>
        /// <param name="user">Usuário a ser cadastrado.</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Post([FromBody] UserDTO user)
        {
            try
            {
                await _adminService.CreateUser(user);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Edita um usuário existente. (Apenas para administradores)
        /// </summary>
        /// <param name="id">Id do usuário</param>
        /// <param name="user">Dados do Usuário</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Put(string id, [FromBody] UserDTO user)
        {
            try
            {
                await _adminService.UpdateUser(id, user);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Exclui um usuário do sistema.  (Apenas para administradores)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Delete(string id)
        {
            try
            {
                await _adminService.DeleteUser(id);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

    }
}
