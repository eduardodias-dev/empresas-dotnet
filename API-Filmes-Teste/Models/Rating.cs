﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_Filmes_Teste.Models
{
    public class Rating
    {
        [Key]
        public int RatingId { get; set; }
        [Required]
        public string UserId { get; set; }
        public IdentityUser User { get; set; }
        [Required]
        public int MovieId { get; set; }
        public Movie Movie { get; set; }
        [Range(0,4)]
        public int Rate { get; set; }
        public bool Deleted { get; set; }
    }
}
