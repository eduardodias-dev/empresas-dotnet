﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_Filmes_Teste.Models
{
    public class Movie
    {
        [Key]
        public int MovieId { get; set; }
        public string Name{ get;set;}
        public string Description { get; set; }
        public string Director { get; set; }
        public string Genre { get; set; }
        public ICollection<Rating> Ratings { get; set; }
        public bool Deleted { get; set; }
        public Movie()
        {
            Ratings = new HashSet<Rating>();
        }
        public double AverageRating { get
            {
                return Ratings.Count > 0 ? Ratings.Average(x => x.Rate) : 0;
            } }

    }
}
