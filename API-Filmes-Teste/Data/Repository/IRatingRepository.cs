﻿using API_Filmes_Teste.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Filmes_Teste.Data.Repository
{
    public interface IRatingRepository : IRepository<Rating>
    {
    }
}
