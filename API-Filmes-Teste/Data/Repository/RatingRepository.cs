﻿using API_Filmes_Teste.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Filmes_Teste.Data.Repository
{
    public class RatingRepository : Repository<Rating>, IRatingRepository
    {
        public RatingRepository(MovieDbContext context): base(context){}

        public override void Remove(Rating entity)
        {
            entity.Deleted = true;
            _context.Entry(entity).State = EntityState.Modified;
        }
    }
}
