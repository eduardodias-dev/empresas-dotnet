﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace API_Filmes_Teste.Data.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected readonly MovieDbContext _context;
        public Repository(MovieDbContext context)
        {
            _context = context;
        }
        public void Add(T entity)
        {
            _context.Set<T>().Add(entity);
        }

        public void Commit()
        {
            _context.SaveChanges();
        }

        public IQueryable<T> Get()
        {
            return _context.Set<T>().AsQueryable();
        }

        public T GetByExpression(Expression<Func<T, bool>> expression)
        {
            return _context.Set<T>().FirstOrDefault(expression);
        }

        public virtual void Remove(T entity)
        {
            throw new NotImplementedException();
        }

        public void Update(T entity)
        {
            _context.Set<T>().Update(entity);
        }

    }
}
