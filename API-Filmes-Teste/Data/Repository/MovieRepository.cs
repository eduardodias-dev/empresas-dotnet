﻿using API_Filmes_Teste.Models;
using API_Filmes_Teste.Pagination;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace API_Filmes_Teste.Data.Repository
{
    public class MovieRepository : Repository<Movie>, IMovieRepository
    {
        public MovieRepository(MovieDbContext context):base(context){}

        public PagedList<Movie> GetAllMoviesPaged(PageParameters parameters)
        {
            return PagedList<Movie>.ToPagedList(Get(), parameters.PageNumber, parameters.PageSize);
        }

        public Movie GetMovieDetail(Expression<Func<Movie, bool>> expression)
        {
            return _context.Movies.Include(x => x.Ratings).FirstOrDefault(expression);
        }

        public override void Remove(Movie entity)
        {
            entity.Deleted = true;
            _context.Entry(entity).State = EntityState.Modified;
        }
    }
}
