﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace API_Filmes_Teste.Data.Repository
{
    public interface IRepository<T>
    {
        IQueryable<T> Get();
        T GetByExpression(Expression<Func<T, bool>> expression);
        void Add(T entity);
        void Update(T entity);
        void Remove(T entity);
        void Commit();
    }
}
