﻿using API_Filmes_Teste.Models;
using API_Filmes_Teste.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace API_Filmes_Teste.Data.Repository
{
    public interface IMovieRepository : IRepository<Movie>
    {

        PagedList<Movie> GetAllMoviesPaged(PageParameters parameters);
        Movie GetMovieDetail(Expression<Func<Movie, bool>> expression);
    }
}
