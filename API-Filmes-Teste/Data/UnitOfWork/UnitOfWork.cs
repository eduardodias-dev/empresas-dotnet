﻿using API_Filmes_Teste.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Filmes_Teste.Data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly MovieDbContext _context;
        public UnitOfWork(MovieDbContext context)
        {
            _context = context;
        }

        private IRatingRepository _ratingRepo;
        private IMovieRepository _movieRepo;

        public IMovieRepository MovieRepository { 
            get 
            {
                return _movieRepo = _movieRepo ?? new MovieRepository(_context);
            } 
        }

        public IRatingRepository RatingRepository
        {
            get
            {
                return _ratingRepo = _ratingRepo ?? new RatingRepository(_context);
            }
        }

        public void Commit()
        {
            _context.SaveChanges();
        }
    }
}
